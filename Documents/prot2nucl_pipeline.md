# 1 get accessions list from fasta file
+ file : prot_hppd.fasta

+ ex : >A6X7C4_OCHA4/286-422 -> A6X7C4_OCHA4
; ...
aide :
- http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc11

# 2 mapping uniprot to EMBL :
+ http://www.uniprot.org/uploadlists/
+ UnirotKB to EMBL/GenBank/DDBJ CDS

+ ex : A6X7C4_OCHA4 -> ABS17128.1

Aide :

install bioservices:
- https://bioconda.github.io/index.html#set-up-channels
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
conda install bioservices

Mapping : 
+ https://bioservices.readthedocs.io/en/master/quickstart.html#uniprot-service

Sources :
- https://bioservices.readthedocs.io/en/master/index.html
- https://bioinfo-fr.net/bioservices-module-python 


# 3 get genbank file from EMBL accessions list 
+ https://www.ncbi.nlm.nih.gov/genbank/

aide :
+ https://bioservices.readthedocs.io/en/master/references.html#module-bioservices.eutils

# 4 get CDS coded_by feature and extract region start and stop positions
Ex : 
LOCUS       ABS17128                 633 aa            linear   BCT 28-JAN-2014
DEFINITION  4-hydroxyphenylpyruvate dioxygenase [Ochrobactrum anthropi ATCC
            49188].
ACCESSION   ABS17128
VERSION     ABS17128.1
DBLINK      BioProject: PRJNA19485
            BioSample: SAMN02598421
DBSOURCE    accession CP000759.1

...
     CDS             1..633
                     /locus_tag="Oant_4428"
                     /coded_by="complement(CP000759.1:1861699..1863600)"
                     /note="PFAM: Glyoxalase/bleomycin resistance
                     protein/dioxygenase; Xylose isomerase domain protein TIM
                     barrel;
                     KEGG: ret:RHE_PC00215 putative 4-hydroxyphenylpyruvate
                     dioxygenase protein"
                     /transl_table=11
                     /db_xref="InterPro:IPR004360"
                     /db_xref="InterPro:IPR012307"
...
//


+ get data: CP000759.1 (nucleotide accession) , start = 1861699, stop = 1863600 (+1)
for each EMBL accessions

+ get nucleotide accessions list

Aide :
+ http://biopython.org/DIST/docs/tutorial/Tutorial.html#sec:seq_features
+ 4.3.2.4  Location testing

# 5 get nucleotide fasta file from CDS accessions list

ex : 
>CP000759.1 
ATATTGCTTGACGT....

Aide :
+ https://bioservices.readthedocs.io/en/master/references.html#module-bioservices.eutils

# 6 for each EMBL extract the nucleotide region and get out the fasta file

Aide :
+ http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc33 
+ http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc19
+ http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc23


