#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, os.path
import xml.etree.ElementTree as ET
import argparse

def main(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(description="Create a tree of directory for a ipm project. Usage : %prog -s structure.xml -n name")
    parser.add_argument("-r", "--root", dest="root", default=".",
; ; ; ; help="Specify the basedir of the project")
    parser.add_argument("-n", "--name", dest="project_name", default="myProject",
; ; ; ; help="The project name and root directory")
    parser.add_argument("-s", "--structure", dest="xml_tree", default=None,
; ; ; ; help="Specify the xml structure file of the project")
    args = parser.parse_args()
    if args.project_name:
        slash = os.path.abspath(os.path.join(args.root,args.project_name))
    else:
        slash = os.path.abspath(os.path.join(args.root,args.tree_root.get('name')))
    if args.xml_tree:
        f_arborescence_projet = os.path.abspath(args.xml_tree)
        element = ET.parse(f_arborescence_projet)
    else:
        f_arborescence_projet = """<?xml version='1.0'?>
         <structure>
            <dir name="bin" category="appli">
            <dir name="ext" />
            <dir name="int" />
            </dir>
            <dir name="doc" category="doc"/>
            <dir name="data" category="data">
            <dir name="ext" />
            <dir name="int" />
            <dir name="out" />
            <dir name="tmp" />
            </dir>
            <dir name="Notebooks" category="appli">
            </dir>
            </structure>""".format(project = slash)
        element = ET.fromstring(f_arborescence_projet)
    tree = ET.ElementTree(element)
    tree_root = tree.getroot()

    print "Root : {0:s}".format(slash)
    if not os.path.isdir(slash):
        os.mkdir(slash)
    else:
        print slash  + " already exists"
    create_structure(tree_root,basedir = slash)


### Functions ###

def create_structure(element, basedir = None):
    """Creates directory structure from XML."""
    list_dir = element.findall("dir")
    for i, each in enumerate(list_dir):
        oNewDir = os.path.join(basedir, each.get('name'))
        if not os.path.isdir(oNewDir):
            os.mkdir(oNewDir)
            #print oNewDir
        else:
            print oNewDir + " already exists"
        create_structure(each, basedir = oNewDir)
    return

def print_structure(element, basedir = None):
    """Print directory structure from XML."""
    list_dir = element.iter("dir")
    for each in list_dir:
        oNewDir = os.path.join(basedir, each.get('name'))
        if not os.path.isdir(oNewDir):
            #os.mkdir(oNewDir)
            print  oNewDir
        else:
            print oNewDir + " already exists"
    return

if __name__ == "__main__":
    sys.exit(main())
